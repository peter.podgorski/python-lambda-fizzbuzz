from typing import Dict

point_map: Dict[str, int] = {
    "a": 1,
    "b": 3,
    "c": 3,
    "d": 2,
    "e": 1,
    "f": 4,
    "g": 2,
    "h": 4,
    "i": 9,
    "j": 8,
    "k": 5,
    "l": 4,
    "m": 3,
    "n": 1,
    "o": 1,
    "p": 3,
    "q": 10,
    "r": 1,
    "s": 1,
    "t": 1,
    "u": 1,
    "v": 4,
    "w": 4,
    "x": 8,
    "y": 4,
    "z": 10,
}


def functional(word: str, points_per_letter: Dict[str, int]):
    return (
        sum(map(points_per_letter.get, word))
        + max(map(points_per_letter.get, set(word)))
        + (40 * len(word) == 7)
    )


def list_comprehension(word: str, points_per_letter: Dict[str, int]):
    return (
        sum(points_per_letter[letter] for letter in word)
        + max(points_per_letter[letter] for letter in set(word))
        + (40 * len(word) == 7)
    )


def classic(word: str):
    max_points_for_letter = 0
    points = 0
    for letter in word:
        points_for_letter = point_map[letter]
        if points_for_letter > max_points_for_letter:
            max_points_for_letter = points_for_letter
        points += points_for_letter
    if len(word) == 7:
        points += 40
    points += max_points_for_letter
    return points


print('Classic', classic('scrabble'))
print('List comprehension', list_comprehension('scrabble', point_map))
print('Functional', functional('scrabble', point_map))
