def split_list(text, separator):
    chunks = []
    while separator in text:
        sep_pos = text.find(separator)
        chunk = text[:sep_pos]
        text = text[sep_pos + 1:]
        chunks.append(chunk)
    chunks.append(text)
    return chunks


def split_gen(text, separator):
    while separator in text:
        sep_pos = text.find(separator)
        chunk = text[:sep_pos]
        text = text[sep_pos + 1:]
        yield chunk
    yield text


def split_func(text, separator):
    sep_pos = text.find(separator)
    chunk = text[:sep_pos]
    text = text[sep_pos + 1:]
    yield chunk
    if separator in text:
        yield from split_func(text, separator)
    else:
        yield text


text = '123/111/333'
separator = '/'
assert split_list(text, separator) == text.split(separator), split_list(text, separator)
assert list(split_gen(text, separator)) == text.split(separator), list(split_gen(text, separator))
assert list(split_func(text, separator)) == text.split(separator), list(split_func(text, separator))
