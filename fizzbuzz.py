lambda n: \
    'Fizz' * (not (n % 3)) + \
    'Buzz' * (not (n % 5)) + \
    str(n) * bool(n % 3 and n % 5)